#!/usr/bin/env bash

gnuplot << EOF
set terminal postscript portrait enhanced color dashed lw 1 "DejaVuSans" 12
set output "temp.ps"
set title "ESP Latency with load - effect of incorrectly set priorities"
set logscale y
set grid
set xlabel "Time [us]"
set ylabel "Latency profile [messages]"
plot "automated-speed125000-floodFalse-frameLength2-messagesCount3200-loadTrue-id0004-OtocenyPriority-2022-04-14T13:12:34.892359-hist.txt.dat" using 1:2 with steps title "NoFlood,Len2",\
     "automated-speed125000-floodFalse-frameLength4-messagesCount3200-loadTrue-id0004-OtocenyPriority-2022-04-14T13:12:41.199330-hist.txt.dat" using 1:2 with steps title "NoFlood,Len4",\
     "automated-speed125000-floodFalse-frameLength8-messagesCount3200-loadTrue-id0004-OtocenyPriority-2022-04-14T13:12:48.464143-hist.txt.dat" using 1:2 with steps title "NoFlood,Len8",\
     "automated-speed125000-floodTrue-frameLength2-messagesCount3200-loadTrue-id0004-OtocenyPriority-2022-04-14T13:12:13.987681-hist.txt.dat" using 1:2 with steps title "Flood,Len2",\
     "automated-speed125000-floodTrue-frameLength4-messagesCount3200-loadTrue-id0004-OtocenyPriority-2022-04-14T13:12:20.285619-hist.txt.dat" using 1:2 with steps title "Flood,Len4",\
     "automated-speed125000-floodTrue-frameLength8-messagesCount3200-loadTrue-id0004-OtocenyPriority-2022-04-14T13:12:26.644456-hist.txt.dat" using 1:2 with steps title "Flood,Len8"
EOF
epstopdf temp.ps
pdfcrop temp.pdf; mv temp-crop.pdf inverse_priorities_only.pdf

gnuplot << EOF
set terminal postscript portrait enhanced color dashed lw 1 "DejaVuSans" 12
set output "temp.ps"
set title "ESP Latency with load - correctly set priorities"
set logscale y
set grid
set xlabel "Time [us]"
set ylabel "Latency profile [messages]"
plot "automated-speed125000-floodFalse-frameLength2-messagesCount3200-loadTrue-id0004-2022-04-14T13:09:56.406937-hist.txt.dat" using 1:2 with steps title "NoFlood,Len2",\
     "automated-speed125000-floodFalse-frameLength4-messagesCount3200-loadTrue-id0004-2022-04-14T13:10:02.670315-hist.txt.dat" using 1:2 with steps title "NoFlood,Len4",\
     "automated-speed125000-floodFalse-frameLength8-messagesCount3200-loadTrue-id0004-2022-04-14T13:10:09.880362-hist.txt.dat" using 1:2 with steps title "NoFlood,Len8",\
     "automated-speed125000-floodTrue-frameLength2-messagesCount3200-loadTrue-id0004-2022-04-14T13:09:35.534327-hist.txt.dat" using 1:2 with steps title "Flood,Len2",\
     "automated-speed125000-floodTrue-frameLength4-messagesCount3200-loadTrue-id0004-2022-04-14T13:09:41.769979-hist.txt.dat" using 1:2 with steps title "Flood,Len4",\
     "automated-speed125000-floodTrue-frameLength8-messagesCount3200-loadTrue-id0004-2022-04-14T13:09:48.114308-hist.txt.dat" using 1:2 with steps title "Flood,Len8"
EOF
epstopdf temp.ps
pdfcrop temp.pdf; mv temp-crop.pdf correct_priorities_only.pdf

gnuplot << EOF
set terminal postscript portrait enhanced color dashed lw 1 "DejaVuSans" 12
set output "temp.ps"
set title "ESP Latency with load - show how incorrect priority affects latency"
set logscale y
set grid
set xlabel "Time [us]"
set ylabel "Latency profile [messages]"
plot "automated-speed125000-floodFalse-frameLength2-messagesCount3200-loadTrue-id0004-2022-04-14T13:09:56.406937-hist.txt.dat" using 1:2 with steps title "Correct priorities",\
     "automated-speed125000-floodFalse-frameLength2-messagesCount3200-loadTrue-id0004-OtocenyPriority-2022-04-14T13:12:34.892359-hist.txt.dat" using 1:2 with steps title "Inverse priorities"
EOF
epstopdf temp.ps
pdfcrop temp.pdf; mv temp-crop.pdf compare_priorities.pdf

# automated-speed125000-floodFalse-frameLength2-messagesCount3200-loadTrue-id0004-2022-04-14T13:09:56.406937-hist.txt.dat
# automated-speed125000-floodFalse-frameLength4-messagesCount3200-loadTrue-id0004-2022-04-14T13:10:02.670315-hist.txt.dat
# automated-speed125000-floodFalse-frameLength8-messagesCount3200-loadTrue-id0004-2022-04-14T13:10:09.880362-hist.txt.dat
# automated-speed125000-floodTrue-frameLength2-messagesCount3200-loadTrue-id0004-2022-04-14T13:09:35.534327-hist.txt.dat
# automated-speed125000-floodTrue-frameLength4-messagesCount3200-loadTrue-id0004-2022-04-14T13:09:41.769979-hist.txt.dat
# automated-speed125000-floodTrue-frameLength8-messagesCount3200-loadTrue-id0004-2022-04-14T13:09:48.114308-hist.txt.dat

