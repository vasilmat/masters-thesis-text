\chapter{Running Linux on MZAPO board}
\label{chapter:linux-on-mzapo}

This section describes the setup used to run Linux on the MZAPO boards.
U-Boot bootloader is installed on the board, where it loads the boot instructions from TFTP server, and boots a Linux kernel with NFS mounted root partition.

\begin{figure}
    \includegraphics[width=0.8\linewidth]{figures/uboot-boot-diagram.pdf}
    \caption{Diagram of the booting process. First the U-Boot loads from the TFTP server a single uImage with everything needed to boot, this uImage is extracted and booted, and the booted Linux kernel mounts its root partition from the NFS server.}
    \label{figure:mzapo-pc-boot}
\end{figure}

\section{Das U-Boot}
U-Boot is a bootloader used mainly in embedded applications to boot the operating system (oftentimes Linux)~\cite{uboot}.
Bootlader's other tasks include initializing the CPU, memory controllers and other hardware.
U-Boot's operating system booting routine is composed of multiple steps.
High level overview of the routine is: it loads all the required components (kernel, device tree, initial ramdisk, FPGA bitstream) at explicit memory addresses, sets up kernel boot arguments, and finally boots the kernel.

% U-Boot is installed in the Boot ROM (Read Only Memory), which is a memory location where the CPU program counter points right after power on, so it is the first executed code on a computer.
% Or the Boot ROM contains only small program (first stage bootloader), which in turn loads U-Boot (second stage bootloader).

On MZAPO boards, the U-Boot is installed on SD card inserted into the MicroZed Zynq.
The Boot ROM contains a first-stage bootloader, which is configured using jumpers on the board to boot from SD card (or from internal Flash memory).
This first-stage bootloader then loads another piece of code from the SD card from predefined memory location.
This piece of code then loads the U-Boot SPL (secondary program loader) in a \ctulst!boot.bin! file, which in turn loads the full U-Boot bootloader (second-stage bootloader) from \ctulst!u-boot.img!.
In the SD card filesystem is also an \ctulst!uEnv.txt! configuration file, which contains U-Boot environment variables.

Variables in the \ctulst!uEnv.txt! are used to configure the boot proces.
Listing~\ref{listing:mzapo-uenvtxt} shows the contents of this file.
Variable \ctulst!set_ethaddr! contains command to configure MAC address using the variable \ctulst!ethaddr! (which is automatically picked up by U-Boot)~\cite{uboot:env-vars}.
Setting \ctulst!autoload=no! then disables automatic image loading when running \ctulst!dhcp! (and other) commands.
The \ctulst!dhcp! will then only perform DHCP configuration without automatic image loading (from whatever the configured image source is).
\ctulst!tftpserverip! is a helper variable containing IP address of the TFTP server, in this case it is the IP address of my desktop (running the TFTP server).
This is followed by another two variables \ctulst!bootscript_addr! and \ctulst!bootscript_path!, which contain the memory location (0x01000000) where the bootscript image will be loaded and the bootscript image file address on the TFTP server (\ctulst!/tftp/zynq/autoscr.scr!).

Then there is a collection of variables and \ctulst!use_uart0! to configure serial line to be on the USB Type B connector on expansion board in MZAPO (which is more robust and student-proof connector than microUSB).
Variable \ctulst!sboot! then configures the actual boot procedure used in our setup.
U-boot first obtains IP address with the \ctulst!dhcp! command, then sets \ctulst!serverip! variable (needed for \ctulst!tftp! command which isn't used here, so maybe it could be deleted).
Then the \ctulst!tftpboot! command is executed with parameters \ctulst!bootscript_addr! and \ctulst!tftpserverip!:\ctulst!bootscript_path!, to load the boot script into memory at specified address.
And finally the \ctulst!source! command runs the boot script from given memory address.

At the end is \ctulst!default_bootcmd! variable with the default boot command, which however isn't used, because we also set the \ctulst!uenvcmd!.
This \ctulst!uenvcmd! is picked up by U-Boot, and in the variable we tell the U-Boot to \ctulst!run! the commands in variables \ctulst!set_ethaddr! and \ctulst!sboot!, so U-Boot sets the MAC address and then runs the \ctulst!sboot! series of commands described above.

\begin{lstlisting}[caption={uEnv.txt file used in MZAPO board.},label={listing:mzapo-uenvtxt}]
set_ethaddr=setenv ethaddr 00:0a:35:00:22:02
autoload=no
tftpserverip=192.168.0.110

bootscript_addr=0x01000000
bootscript_path=/tftp/zynq/autoscr.scr

SLCR_UNLOCK=0xF8000008
SLCR_LOCK=0xF8000004
APER_CLK_CTRL=0xF800012C
UART_CLK_CTRL=0xF8000154
MIO_PIN_10=0xF8000728
MIO_PIN_11=0xF800072C

use_uart0=mw.l ${SLCR_UNLOCK} 0xDF0D ; mw.l ${APER_CLK_CTRL} 0x01ff044d ; mw.l ${UART_CLK_CTRL} 0x00001403 ; mw.l ${MIO_PIN_10} 0x16E1 ; mw.l ${MIO_PIN_11} 0x16E0 ; mw.l ${SLCR_LOCK} 0x767B ; setenv stdout ttyPS0 ; setenv stderr ttyPS0 ; setenv stdin ttyPS0

sboot=dhcp && setenv serverip ${tftpserverip} && tftpboot ${bootscript_addr} ${tftpserverip}:${bootscript_path} && source ${bootscript_addr}

default_bootcmd=run use_uart0 set_ethaddr

uenvcmd=run set_ethaddr sboot
\end{lstlisting}

% ---------------------------------------------------------------------------------------------
The autoscr bootscript on TFTP server is a Hush shell (Bourne-like shell used in U-Boot) script.
The script packed into U-boot image format (on the host PC running TFTP server) from text file into script using \ctulst!mkimage! command
\begin{lstlisting}
mkimage -A arm -O linux -T script -C none -a 0 -e 0   -n "autoscr example script"   -d autoscr.txt autoscr.scr
\end{lstlisting}

The \ctulst!mkimage! tool is from U-Boot tools (package \ctulst!u-boot-tools! on Ubuntu).
Arguments have the following interpretation:
\begin{itemize}
\item \ctulst!-A! sets the target architecture to ARM,
\item \ctulst!-O! sets the target operating system we will be booting,
\item \ctulst!-T! sets the compilation image target to \ctulst!script! (it can also be a \ctulst!kernel! for kernel image, \ctulst!fpga! for FPGA image, \ctulst!flat_dt! for flat device tree, \ctulst!firmware! for firmware etc.),
\item \ctulst!-C! sets the compression type,
\item \ctulst!-a! sets the load address (in a hexadecimal number),
\item \ctulst!-e! sets the entry point (in a hexadecimal number),
\item \ctulst!-n! sets the image name,
\item \ctulst!-d! specifies the file with image data (here we are compiling shell script in text file to a “script” type image)
\end{itemize}

Contents of such autoscr script used in this setup are in listing~\ref{listing:mzapo-tftp-autoscr}.
Command \ctulst!echo! just prints some text, same as in regular shells.
The \ctulst!nfsserverip! then sets the NFS server IP address (same as the TFTP server, since both are running on the same desktop), and \ctulst!nfspath! sets the path to the directory used as a root filesystem.
Variable \ctulst!image_img! sets the path to the uImage (a multi file image in the form of flattened device tree) with kernel and everything that is booted packed into a single file.
Then there is a \ctulst!test! whether \ctulst!image_override! is defined, and if it is, the image path is overriden.
Variable \ctulst!image_tftp! then defines a series of commands that load the boot image from the TFTP server into memory at address \ctulst!netstart!.
Then \ctulst!bitstream_unpack! defines another series of commands to prepare the FPGA bitstream and load it into memory.
And \ctulst!boot_now! variable uses \ctulst!bootm! command to boot image from memory.

Finally the kernel arguments are set in the \ctulst!bootargs! variable.
Here is the NFS root definition in \ctulst!nfsroot=IP:path! format.
Also the \ctulst!root=/dev/nfs! to tell the kernel that root partition is mounted over NFS.
Documentation about the NFS root setup for Linux is here~\cite{kernel-doc:nfsroot}.
And at last is executed our boot procedure: load the image from TFTP server, unpack bitstream and load it, and boot the loaded image from memory.

\begin{lstlisting}[caption={Autoscr script on TFTP server.},label={listing:mzapo-tftp-autoscr}]
echo Running autoscr from ftp

setenv tftp_path /tftp/zynq

setenv nfsserverip ${tftpserverip}
setenv nfspath /srv/nfs/debian-armhf

setenv netstart 0x01000000

setenv image_img ${tftp_path}/image.ub
test -n "$image_override" && setenv image_img "$image_override"

setenv image_tftp 'echo === Loading boot image; tftpboot ${netstart} ${tftpserverip}:${image_img}; fdt addr ${netstart}'
setenv bitstream_unpack 'fdt get size filesize /images/fpga@1 data; imxtract ${netstart} fpga@1 ${bitstream_load_address}'
setenv boot_now 'bootm ${netstart}'

setenv bitstream_load_address 0x04000000
setenv bitstream_load 'fpga loadb 0 ${bitstream_load_address} ${filesize}'

setenv bootargs ${bootargs} console=ttyPS0,115200
setenv bootargs ${bootargs} clocksource=ttc_clocksource
setenv bootargs ${bootargs} ip=${ipaddr} root=/dev/nfs ro nfsroot=${nfsserverip}:${nfspath}
setenv bootargs ${bootargs} mzapo_lcdip=yes

run image_tftp bitstream_unpack bitstream_load boot_now
\end{lstlisting}

\subsection{FIT File}

The U-Boot uImage loaded from TFTP server is created from U-Boot FIT (Flattened uImage Tree) file, shown in appendix~\ref{appendix:uboot-its} (it is too long to fit in here).
It's structure is similar to Device Tree.
There is a node for every image needed to boot - the Linux kernel, flattened device tree blob, FPGA bitstream, ramdisk.
Each node has a lot of properties (data, type, compression, architecture and os, what kind of hashes should be calculated, etc.), but here we can focus only on the data property.
This property is path to the data file, which will be used to build the single uImage.
After putting together all the necessary files (compressed kernel, FPGA bitstream, initramfs), the U-Boot FIT image is compiled using
\begin{lstlisting}
mkimage -f uboot-image.its image.ub
\end{lstlisting}
% OPTIONAL: jeste jak to mam ve slozce s tim bordelem co se nemeni, akorat tocim kernel
% a asi nakopirovat skript na vytvoreni initramfs (s tim ze ten initramfs by se klidne mohl vynechat - tak ho vynecham???)
% mozna zminit proc se initramfs pouzival v mzapu predtim - ip adresa, gateway shit

Although this setup is very convoluted and complex, it allows for extreme flexibility.
For example, the same board can be used during Real Time Systems course seminars, to boot VxWorks system by simply changing the \ctulst!bootscript_path! variable in \ctulst!uEnv.txt! on the SD card.

\section{Linux Kernel}
\label{section:linux-kernel}

The kernel used is here is versioned in Zynq RT utils and builds repository\footnote{\url{https://github.com/ppisa/zynq-rt-utils-and-builds}}.
It contains linux kernel config suitable for the MZAPO board, and a Makefile that does all the things neccessary to build the kernel (clones kernel git, builds the kernel, installs modules).
Simply calling \ctulst!make! should suffice to build the kernel.
Optionally, call \ctulst!make KERNEL_GIT_REF=$YOUR_PATH_TO_KERNEL_GIT!
The repository also contains three patches to device tree scripts (setting UART0 as a console output, device tree for system with 4x SJA1000 in programmable logic).
However, those patches are already applied if you use the default kernel\footnote{\url{https://github.com/ppisa/linux-kernel/tree/linux-5.10.y-rt-pi}} from the makefile.

Output of this build is the \ctulst!zImage! file, which is a compressed version of the Linux kernel image.
This is used in the U-Boot FIT image file.
Also, modules from the build in \ctulst!/lib/modules/! path are copied to the NFS root (section~\ref{subsection:nfs-root}).

\subsection{Loading Device Tree Blob Overlays at Runtime}
Because the Linux kernel doesn't offer an interface to load Device Tree Overlays at runtime, an out-of-tree external module has to be used.
Here, the \ctulst!dtbocfg!~\cite{dtbocfg} module is used.
It enables user to load/unload DT overlays from userspace.

To compile dtbocgf, run the commands listed in listing~\ref{listing:dtbocfg-compile}.
The variable \ctulst!KERNEL_SRC_DIR! is generally a path to the build directory of your chosen Linux kernel (or directly the kernel source directory, in case you don't use out-of-tree builds), and \ctulst!ARCH! sets the target architecture to arm.
After this, copy the kernel modules again to the NFS root.

\begin{lstlisting}[caption={Commands to compile dtbocfg.},label={listing:dtbocfg-compile}]
make KERNEL_SRC_DIR=${ZYNQ-RT-UTILS-REPO}/projects/linux/build/arm/zynq/ ARCH=arm
make KERNEL_SRC_DIR=${ZYNQ-RT-UTILS-REPO}/projects/linux/build/arm/zynq/ INSTALL_MOD_PATH=${ZYNQ-RT-UTILS-REPO}/projects/linux/build/arm/zynq-modules/ ARCH=arm modules_install
\end{lstlisting}

The Device Tree overlay used in this project are in repository\footnote{\url{https://gitlab.fel.cvut.cz/canbus/zynq/zynq-can-sja1000-top}}, branch \ctulst!mz_apo-2x-xcan-4x-ctu!.
The overlay can be compiled using \ctulst!mkdtb! script in \ctulst!scripts/! directory.
If you need to (maybe when compiling for a new kernel version/vastly different than the host machine), you can add new include search path using \ctulst!-i! argument to the \ctulst!dtc! compiler in \ctulst!mkdtb! script, pointing to the new kernel's device tree scripts directory.

The same repository contains the \ctulst!upbit! script to upload the bitstream into the FPGA and load Device Tree overlay.

\section{TFTP Server}
Another ingredient in the mix is the TFTP server to serve U-Boot FIT images.
On Ubuntu, a package \ctulst!tftpd! has been used to install a TFTP server.
To configure the TFTP server, edit the \ctulst!/etc/xinetd.d/tftp! configuration file - set the \ctulst!server_args! variable to point to your desired TFTP served directory.
\begin{lstlisting}
server_args = /path/to/your/tftp/directory
\end{lstlisting}
Also, restart the TFTP server to pick up the changes.
\begin{lstlisting}
systemctl reload xinetd.service
systemctl restart xinetd.service
\end{lstlisting}

\section{NFS Root and Debian System Setup}

\subsection{NFS Root}
\label{subsection:nfs-root}

The board is booted into an NFS (Network FileSystem) mounted root filesystem.
This allows us to prolong the SD card lifespan by not even mounting it at runtime (it is used only for booting).
We also avoid memory corruption in case of kernel crashes (can happen since we will be developing/debugging kernel drivers).
Also this setup is overall more flexible (has "unlimited" space unlike the SD card), and allows for e.g. upgrading the Linux system without having to physically remove the SD card, plug it into the host PC and do the upgrade.

On Ubuntu, a NFS server can be installed using the package \ctulst!nfs-kernel-server!.
This server is configured using the \ctulst!etc/exports! file.
Add the following line to the config file:
\begin{lstlisting}
/srv/nfs/debian-armhf   192.168.0.0/24(rw,sync,no_subtree_check,no_root_squash,insecure)
\end{lstlisting}
This exposes the \ctulst!/srv/nfs/debian-armhf! path to all IP addresses on "192.168.0.0/24" subnet with the following options:
\begin{itemize}
    \item \ctulst!rw! - allow both read and write requests to the NFS server
    \item \ctulst!sync! - NFS server responds to requests only after the requested changes have been committed - prevents data loss/corruption
    \item \ctulst!no_subtree_check! - disables security checking whether the accessed subdirectory is in the exported tree
    \item \ctulst!no_root_squash! - gives the root user on client authority to access files on the NFS server as a root too. Manual says this is good for diskless systems, which is our case.
    \item \ctulst!insecure! - allow requests from all ports (not only <1024)
\end{itemize}

And finally export the directory with
\begin{lstlisting}
exportfs -r
\end{lstlisting}

\subsection{Debian Setup}
Finally comes the Debian system setup for the MZAPO board.
We will use Debian because I already have experience with managing Debian/Debian-based distributions, and I can ask my supervisor for help (because he also has experience with Debian).
It is of course possible to use other distributions/solutions (maybe even Yocto/Buildroot), but Debian is very convenient.
The following packages will be needed:
\begin{itemize}
    \item \ctulst!qemu-user-static! - static version of QEMU user emulation binaries
    \item \ctulst!qemu-utils! - QEMU utilities
    \item \ctulst!debootstrap! - tool for bootstrapping a Debian system
    \item \ctulst!debian-archive-keyring! - GnuPG archive keys of the Debian archive
\end{itemize}
The debootstrap basically downloads all the necessary .deb packages, unpacks them into the target directory, chroots to the target directory, and runs the installation and configuration scripts from each package~\cite{debian:debootstrap-guide}.
Because we are targeting arm architecture, we will need QEMU for the last step to execute the arm binaries.
This can be also called CrossDebootstrap.

Execute the debootstrap command with the following arguments:
\begin{lstlisting}
/usr/sbin/debootstrap \
    --no-merged-usr \
    --keyring=/usr/share/keyrings/debian-archive-keyring.gpg \
    --arch=armhf \
    --include=debian-keyring,mc,libc6-dev,libstdc++6,busybox,aptitude,etckeeper \
    buster /srv/nfs/debian-armhf http://ftp.cz.debian.org/debian/
\end{lstlisting}
This creates a Debian system (\ctulst!buster! release) in the \ctulst!/srv/nfs/debian-armhf! directory, for target architecture armhf (hf - hard float - for processors with hardware floating point support).
Additionally the packages \ctulst!debian-keyring!, \ctulst!mc!, \ctulst!libc6-dev!... will be installed on the target system.

After the installation finishes, one more configuration has to be done.
Some packages may contain system services, which are started post installation under normal circumstances.
Such services are also restarted on package upgrades.
But if we are chrooted inside the newly created system and install such a package, it will try to start the service, which is undesirable.
Solution to this is to create a \ctulst!/srv/nfs/debian-armhf/usr/sbin/policy-rc.d! script inside the newly created system, listing~\ref{listing:policy-rc} shows the script used in this installation.
It checks the hostname if it is equal to the chroot host computer hostname, and returns code 101 in such case.
Error code means "action is denied", so when you install something from chroot on the host computer, it won't try to manage the services.
If you install something from the system running on the MZAPO board, the services will be managed as usual.
Also change the hostname in \ctulst!/etc/hostname! in the new system, otherwise the script won't work.

Finally, don't forget to copy the kernel modules, as mentioned in section~\ref{section:linux-kernel}.

\begin{lstlisting}[caption={},label={listing:policy-rc}]
#!/bin/sh
[ "$(hostname)" = "$USE_HERE_THE_HOSTNAME_OF_THE_CHROOT_HOST" ] && exit 101
exit 0
\end{lstlisting}