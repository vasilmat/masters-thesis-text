\chapter{Timestamping in Linux Drivers}
\label{chapter:timestamping}

Hardware timestamps are time marks stored inside the CAN controller whenever a message is sent or received (depends on what the controller supports).
The controller typically has an internal counter, which is sampled when an event happens (send/receive).
This counter value (a.k.a. the timestamp) is then stored in some buffer along the frame data.

In order to achieve better precision of measured latencies, it is necessary to use hardware timestamps provided by the CAN bus controller.
The alternative - using software timestamps from the kernel, stored when the frame gets to the operating system networking stack - is worse for multiple reasons.
And software timestamping in the userspace is consequently even worse.
The hardware timestamp is more precise, because it is stored exactly when the CAN frame is sent/received on the bus.
Software timestamp stored in the kernel might or might not be close to real value, due to the processing as the frame bubbles up the kernel software stack.
In fact, it can happen that even though a CAN message $A$ was received before another CAN message $B$, the software timestamp $t_A$ is greater (later in time) than timestamp $t_B$.
Because the interrupt handlers (defined in a device driver) are preemtible, and the software timestamps are generated only after the packet is handed from the device driver to the kernel stack~\cite{kernel-doc:networking-timestamping}, two frames received immediately one by one can have reverse timestamps (the second interrupt preemts the first, finishes, and then the kernel might continue the first interrupt/continue with the second message and generate a timestamp).

\section{Timestamps from Xilinx CAN}

In the Xilinx CAN controller included in the MicroZed Zynq board~\cite{avnet-microzed}, the timestamping counter is only 16 bits wide.
This means that the counter will rollover very often.
Quick calculation: assuming the CAN controller runs on 100 MHz clock, the counter will roll over every
$$ {2^{16} \over 100 \cdot 10^6} = 0.6~\rm ms, $$
which is very often.

To further complicate matters, the timestamping counter is not readable.
If it was readable, one could set up a kernel worker to periodically read the counter and keep track of its state and rollovers.
However, spawning a task with period $ T < 0.6~\rm ms $ doesn't sound like a great idea either, and might not be possible on some constrained systems.

To convert the 16-bit timestamp to real time value and make use of the hardware timestamping, Martin Jerabek came up with an conversion algorithm is his bachelor thesis~\cite{jerabek-bc}.
The algorithm is show in listing~\ref{listing:jerabek-alg1}.

\begin{lstlisting}[caption={First iteration of M. Jerabek's algorithm for converting Xilinx CAN 16-bit timestamps to real time~\cite{jerabek-bc}.},label={listing:jerabek-alg1}]
def convert(unsigned int timestamp, reference_time_point ref):
    // ktime is abbreviation for kernel time
    ktime_now = ktime_get_real_ns()
    if (this CAN frame is the first received):
        set the reference point to now
        ref->ktime = ktime_now
        ref->tstamp = timestamp
        return ktime_now
    else:
        // calculate the number of rollovers since the reference
        rollovers = (ktime_now - ref->ktime) / counter_rollover_time_ns
        // C is a constant to convert from counter time to nanoseconds
        // C = NSEC_PER_SEC / counter_frequency
        timestamp_delta_ns = (timestamp - ref->ts) * C
        return ref->ktime
            + rollovers * counter_time_rollover_ns
            + timestamp_delta_ns
\end{lstlisting}

The algorithm is invoked from the function handling the received CAN frame (which itself is invoked from the Interrupt Service Routine (ISR)), and works as follows.
If the received CAN frame is the first frame received, the reference time point is set to current kernel real time, together with the timestamping counter value associated with it, and the current kernel time is returned.
If the received frame isn't the first: the number of timestamping counter rollovers since the reference time point is calculated.
Then is calculated the difference between reference and current timestamp, and converted to nanoseconds.
Finally we can return the reference kernel time, plus the number of rollovers times the time one rollover takes, plus the timestamps difference in seconds.
Listing~\ref{listing:kernel-xilinx-isr} shows simplified ISR handling function from the Linux Xilinx driver for context, source taken from\footnote{\url{https://elixir.bootlin.com/linux/v5.18-rc4/source/drivers/net/can/xilinx\_can.c\#L741}}.
\begin{lstlisting}[caption={Simplified function handling received frames called from the ISR, with timestamp extraction from DLC register.},label={listing:kernel-xilinx-isr}]
static int xcan_rx(struct net_device *ndev, int frame_base) {
    allocate socket buffer
    read from the RX FIFO
        data length code (DLC) register
        extract timestamp from the DLC register
        get pointer to the buffer timestamp storing struct
        convert timestamp and store it
    some processing to match SocketCAN format
    pass the socket buffer up in the kernel networking stack
}
\end{lstlisting}

The reference time point can't be dynamically updated.
That would require some synchronization primitive (spinlock/mutex) to avoid bugs when tow interrupts want to update the reference (remember, interrupts can be preemted), and such synchronization is not worth it (introducing synchronization and thus slowing down processing only to guarantee correct timestamps).
Even the current implementation deserves some synchronization when setting the reference, but again, it is rather not worth it.
We can live with the first/first few timestamp being off, or the reference kernel time point being inaccurate by a small amount (sub-millisecond error).

\subsection{Final Timestamping Version}
However, the algorithm listed in~\ref{listing:jerabek-alg1} “suffered slight problems at hardware counter overflows”, as the author reports in~\cite{jerabek-bc}.
Therefore a new version has been developed.

\begin{lstlisting}[caption={Fixed algorithm for converting 16-bit timestamps from Xilinx CAN controller~\cite{jerabek-bc}.},label={listing:jerabek-alg2}]
ktime_t get_frame_timestamp(u16 frame_cantime, ktime_t frame_ktime) {
    if first frame
        ref_ktime = frame_ktime
        ref_cantime = frame_cantime
        exact_frame_ktime = frame_ktime;
    else
        frame_cantime_full = ktime_to_cantime(frame_ktime) - ref_cantime
        replace the lower 16 bits of frame_cantime_full by frame_cantime
        frame_cantime_full += ref_cantime
        exact_frame_ktime = cantime_to_ktime(frame_cantime_full)
    return exact_frame_ktime
}
\end{lstlisting}

The new algorithm is in listing~\ref{listing:jerabek-alg2}.
If the received frame is the first frame, it returns the current kernel real time, same as in the first algorithm version.
Else, the current kernel time (\ctulst!frame_ktime!) is converted to “frame\_cantime\_full” (as if the counter could count to infinity and didn't wrap around at $2^{16}$).
The \ctulst!ktime_to_cantime! conversion is multiplying by $ counter\_frequency / NSEC\_PER\_SEC $ constant, and \ctulst!cantime_to_ktime! is its inverse.
Then the 16 least significant bits of this \ctulst!frame_cantime_full! are replaced by the hardware timestamp of current frame.
Finally the reference timestamp is added and the full frame cantime is converted back to real time.
% To aid the explanation, figure~\ref{figure:znazorneni-jerabek-alg2} shows a graph showing both of the times.

% \begin{figure}
%     \includegraphics[width=0.8\linewidth]{figures/cmelak1.jpg}
%     \caption{Graph of the algorithm and times used in the conversion algorithm~\ref{listing:jerabek-alg2}.}
%     \label{figurefigure:znazorneni-jerabek-alg2}
% \end{figure}
% ASI NIC: udelat obrazek s tim pilovitym diagramem

Both of the algorithms were implemented by Martin Jerabek in the Xilinx CAN Linux driver.
My work has been to rebase the fixed algorithm on latest Linux kernel (since the patch was over 5 years old), polish it and prepare it for submission to Linux.
The patch is in repository\footnote{\url{https://gitlab.fel.cvut.cz/vasilmat/linux-can-test}}, and hopefully will be accepted by the maintainers.
I've also tried to implement my conversion algorithm based on a consultation with my supervisor Pavel Pisa, but it turned out to be virtually the same as the first version from M. Jerabek~\cite{jerabek-bc}, so I have abandoned it.

\section{Timestamps in CTU CAN-FD IP core}
With CTU CAN FD IP cores, the timestamping situation is much more interesting (and feasible).
The CTU CAN FD has support for timestamps on received frames (configurably both at start of frame or end of frame) and supports even time trigerred transmission.
By default RX timestamps are sampled at the end of frame~\cite{ctu-can-fd-ip-core:datasheet}.
The IP core is designed in such a way that timestamps are provided by the system integrator of the IP core - the integrator provides an up-counting counter (up to 64-bits wide) and connects it to the \ctulst!timestamp! input of the IP core.
This design allows all the IP cores in a single design to share a single time base~\cite{ctu-can-fd-ip-core:system-architecture}.

There has been preliminary research on timestamps implementation done by Martin Jerabek in\footnote{\url{https://gitlab.fel.cvut.cz/canbus/ctucanfd_ip_core/-/issues/158}}.
There is a question what to do when the provided timestamps counter is not 64-bits wide.
He came to the conclusion that the counter signal should be right-aligned, since we gain nothing by left-aligning and it is even counter intuitive.
Which is a good conclusion and we will continue with it.
The timestamping frequency will be configurable in two ways: you can pass a second clock phandle in the \ctulst!clocks! property in Device Tree, or you can pass directly a number as a \ctulst!ts_frequency! property.

\subsection{Quick Introduction to Device Tree}
This subsection contains a brief introduction to Device Tree, what it is and how it helps Linux users/developers.

Device Tree is a system (in the form of a tree data structure) for describing non-discoverable hardware~\cite{elinux:device_tree_reference}.
Because not every bus supports device enumeration (e.g. PCI/PCIe support enumeration, APB and Avalon don't), the kernel source code had to contain full description of the hardware.
That includes even register addresses, how many cores given CPU has, how much memory is available in the system, and so on.
Now with Device Tree you can describe your hardware in the form of Device Tree Script (dts), compile it to Device Tree Blob (dtb) using the Device Tree Compiler, and then the Linux kernel can load this blob during boot.
Alternatively, you can load an Device Tree Blob Overlay during run time, using the \ctulst!dtbocfg! kernel module~\cite{dtbocfg}.
Listing~\ref{listing:device-tree-example} shows an example Device Tree script.

As you can see, there are two nodes \ctulst!node1! and \ctulst!node2!, both with a bunch of properties in the form of key-value pairs.
Device Tree supports a few data types for the propeties:
\begin{itemize}
    \item String - \ctulst!A string!
    \item Cell - a list of 32-bit unsigned integers inside angle brackets - $\rm \langle 123~0x1A~\&clock \rangle$
    \item Comma separated list of strings - “first string”, “second string”
\end{itemize}
There is also the \ctulst!phandle! type used inside cells (e.g. \ctulst!clocks = <&clkc 15>;!), which is a reference to another node in the device tree.
Phandle value itself is a 32-bit unsigned integer, a number referring to another node.
In the example above, \ctulst!&clkc! refers to the with label \ctulst!clkc! (which actually defines clocks and clock names for MicroZed Zynq 7000 SoC based boards), and the device tree compiler would resolve the \ctulst!&clkc! phandle to the numerical value of the \ctulst!clkc! label.
There is much more going on in the Device Tree, but we will not go into details here, since we don't need to.
For more information about Device Tree, you can refer to the specification~\cite{device_tree_specification} or some of the wiki pages on elinux.org~\cite{elinux:device_tree_reference},\cite{elinux:device_tree_usage}.

\begin{lstlisting}[caption={Example Device Tree Script, taken from~\cite{elinux:device_tree_usage}.},label={listing:device-tree-example}]
/dts-v1/;

/ {
    node1 {
        a-string-property = "A string";
        a-string-list-property = "first string", "second string";
        // hex is implied in byte arrays. no '0x' prefix is required
        a-byte-data-property = [01 23 34 56];
        child-node1 {
            first-child-property;
            second-child-property = <1>;
            a-string-property = "Hello, world";
        };
        child-node2 {
        };
    };
    node2 {
        an-empty-property;
        a-cell-property = <1 2 3 4>; /* each number (cell) is a uint32 */
        child-node1 {
        };
    };
};
\end{lstlisting}

\subsection{Common Clock Framework in Linux}
Before we get to the timestamping patch itself, there should be few words about the Common Clock Framework in the Linux kernel, since we will make use of it when setting up timestamping clock.
The common clock framework was introduced in 2012 to unify the API across a variety of platforms~\cite{kernel-doc:ccf}.
It provides means to control the clock nodes (getting/setting the clock rate, phase), implement custom clock drivers and much more.
But again, we will use only tiny subset of the API - specifically obtaining the clock rate and letting the framework setup the timestaping clock for us using a definition in Device Tree.

One of the main benefits of the framework for us is that it enables clocks to be declared from the Device Tree.
The framework parses the device tree and sets up the clocks.
Figure~\ref{figure:clock-overview} shows the clock framework architecture - we are in the “Kernel space” part, specifically the user driver.
For the clock consumer (us) it is possible to only declare the used clocks in \ctulst!clocks! property in Device Tree for our hardware.
Then in the driver, we obtain a clock reference using \ctulst!clk_get!/\ctulst!devm_clk_get! (devm version returns a managed reference to the clock producer - it will be automatically freed when the device is removed~\cite{kernel-doc:devm}).
Then we prepare the clock using \ctulst!clk_prepare! and enable it with \ctulst!clk_enable!.
Listing~\ref{listing:clock-framework-usage} shows some basic example of common clock framework usage (without error handling etc.).

\begin{figure}
    \includegraphics[width=\linewidth]{figures/Clock_Overview.png}
    \caption{Graphical overview of the clock framework. Figure taken from~\cite{st32:clock-overview}.}
    \label{figure:clock-overview}
\end{figure}

However, it is not necessary to call those functions manually, because the current version of CTU CAN FD Linux driver uses\footnote{\url{https://git.kernel.org/pub/scm/linux/kernel/git/mkl/linux-can-next.git/tree/drivers/net/can/ctucanfd/ctucanfd\_base.c\#n1431}} Runtime Power Management framework for I/O devices~\cite{kernel-doc:runtime-pm}.
This framework takes care of the clock management (among other things).
You just implement suspend and resume callback functions, and register those callbacks in \ctulst!dev_pm_ops! structure (using some macro, e.g. \ctulst!SIMPLE_DEV_PM_OPS!).

\begin{lstlisting}[caption={Example usage of the Common Clock Framework.},label={listing:clock-framework-usage}]
struct clk *clk;
u32 frequency;
clk = devm_clk_get(dev, "timestamping_clock");
frequency = clk_get_rate(clk);

clk_prepare(clk);
clk_enable(clk);
\end{lstlisting}

\subsection{Timecounters and cyclecounters}

Because the timestamps implementation in CTU CAN FD IP core isn't exactly defined and depends on what the system integrator provides, the Linux driver must be able to deal with smaller counters, which will wrap around.
To put things in perspective: assuming 100 MHz timestamping counter frequency, 64-bit wide counter would wrap in almost 6000 years (so we wouldn't have to deal with rollovers, because they won't happen in a lifetime), while 32-bit counter wraps around every 42 seconds.
As per the advice from Marc Kleine-Budde\footnote{\url{https://marc.info/?l=linux-can&m=163472962426850}} (the maintainer of the Linux CAN networking subsystem), the timestamping patch has been implemented using \ctulst!struct timecounter! and \ctulst!struct cyclecounter! helper structures.
% samotna implementace v driveru podle navodu MKB - popsat cyclecountery, timecountery, ze jednotlive zarizeni nejsou synchronizovana, jak se timestampy ctou v userspace

Cyclecounter is the lower-level structure used for abstracting the real free running counter~\cite{kernel-source:timecounter-cyclecounter}.
It has no state, the struct provides only read access to the underlying counter, and helper constants/variables to convert read timestamp to nanoseconds.
Listing~\ref{listing:cyclecounter-definition} shows structure definition taken from the Linux kernel source.
Property \ctulst!mask! is a bitmask for two's complement subtraction (for non-64bit wide counters).
Using the equation below, \ctulst!mult! and \ctulst!shift! properties serve for the actual conversion to nanoseconds, see function \ctulst!cyclecounter_cyc2ns!.
Cycles is the number of counter ticks we want to convert to nanoseconds.
$$
nanoseconds = (cycles * mult) >> shift
$$

The calculation is in fixed point arithmetics, because the FPU's (Floating Point Unit) registers aren't saved when context is switched from userspace to kernel space.
So if we were to use floating point operations, we would destroy userspace data.
It is possible to save the floating point registers and do FP calculations, but it is not neccessary here, as the calculation is rather simple.
Shift is used to avoid precision loss when calculating \ctulst!mult! constant, e.g. when the frequency is about the same order as $10^9$ (nanoseconds per second).
E.g. with frequency 133 MHz you would get $ {10^9 \over 133*10^6} = 7.5 \doteq 7$, causing quite the error when converting counter cycles to nanoseconds.
Function \ctulst!clocksource_hz2mult! is used for calculating the multiplication constant from given shift and frequency.
$$
mult = {{10^9 << shift } \over frequency}
$$

\begin{lstlisting}[caption={Struct cyclecounter, source~\cite{kernel-source:timecounter-cyclecounter}.},label={listing:cyclecounter-definition}]
struct cyclecounter {
    u64 (*read)(const struct cyclecounter *cc);
    u64 mask;
    u32 mult;
    u32 shift;
};
\end{lstlisting}

Timecounter is the higher-level structure above cyclecounter.
This structure keeps count of the elapsed nanoseconds since it has been initialized.
Listing~\ref{listing:timecounter-definition} shows the structure definition taken from the Linux kernel.
Property \ctulst!nsec! is where the elapsed nanoseconds are kept.
Property \ctulst!frac! is used for accumulating fractional nanoseconds (since the \ctulst!mult! constant in cyclecounter can be left shifted for better accuracy, the lower masked part of the result is then added to the fractions before shifting back to the right).
And finall the property \ctulst!mask! is a bit mask to maintain fractions as mentioned above.
Regarding timecounter usage, you will mostly want to use function \ctulst!timecounter_cyc2ns!, which converts given counter value to nanoseconds.
Also, users of the \ctulst!timecounter! structure are responsible for reading the counter more often than it wraps around.
\begin{lstlisting}[caption={Struct timecounter, source~\cite{kernel-source:timecounter-cyclecounter}.},label={listing:timecounter-definition}]
struct timecounter {
    const struct cyclecounter *cc;
    u64 cycle_last;
    u64 nsec;
    u64 mask;
    u64 frac;
};
\end{lstlisting}

The timestamps from different CTU CAN FD controllers are not synchronized, i.e. the timestamps from two controllers for some CAN frame will be slightly shifted, even though the CAN frame has been received at the very same time on both controllers.

\subsection{Device Tree Bindings}

The developed driver expects two properties in the Device Tree bindings:
\begin{itemize}
    \item second \ctulst!clocks! phandle or directly \ctulst!ts-frequency! timestamping frequency
    \item \ctulst!ts-used-bits! bit width of the timestamping counter
\end{itemize}

The work delay for the \ctulst!struct timecounter! periodic reads is calculated using the algorithm in listing~\ref{listing:work-delay}.
Formula for the work delay is
\begin{equation*}
    work\_delay\_jiffies = \frac{2^{ts\_used\_bits - 1}}{ts\_frequency}*HZ.
\end{equation*}
HZ is the number of system ticks per second, and the result is in jiffies (jiffy is the kernel unit of time, 1 jiffy = 1 system tick).
Instead of using full $ 2^{ts\_used\_bits} $, the bit width is reduced by one to read the counter roughly twice per its rollover time.
The algorithm works in fixed point arithmetics, so the \ctulst!fls(HZ)! function is used to get the last set bit in the binary representation of the HZ number.
This allows us to shift left as much as possible to get the best achievable precision.

\begin{lstlisting}[caption={Algorithm for work delay calculation from timestamps frequency and counter bit width},label={listing:work-delay}]
u32 jiffies_order = fls(HZ);
u32 max_shift_left = 63 - jiffies_order;
s32 final_shift = (priv->timestamp_bit_size - 1) - max_shift_left;
u64 tmp = div_u64((u64)HZ << max_shift_left, priv->timestamp_freq);

if (final_shift > 0)
    priv->work_delay_jiffies = tmp << final_shift;
else
    priv->work_delay_jiffies = tmp >> -final_shift;

if (priv->work_delay_jiffies == 0)
    return -EINVAL;

priv->work_delay_jiffies = min(priv->work_delay_jiffies, CTUCANFD_MAX_WORK_DELAY_SEC * HZ);

return 0;
\end{lstlisting}

\section{Results and Future Work on Timestamping Patches}
The produced patches are in the linux-can-next mirror repository\footnote{\url{https://gitlab.fel.cvut.cz/vasilmat/linux-can-test}}, in branches \ctulst!xilinx-timestamping! and \ctulst!ctucanfd-timestamping!.

Initial RFC version of the patch for CTU CAN FD driver has been sent for review to the linux-can community\footnote{\url{https://lore.kernel.org/linux-can/20220512232706.24575-1-matej.vasilevski@seznam.cz/t/\#u}}.
Main takeaways from this RFC are:
\begin{itemize}
    \item \ctulst!ts-used-bits! will be (most likely) deleted from the DT bindigs. Instead, the counter width will be read from a register in the IP core.
    \item \ctulst!ts-frequency! will be deleted, only the \ctulst!clocks! phandle will be used.
    \item \ctulst!Kconfig! option enabling the timestamps by default will be deleted, instead the \ctulst!SIOCSHWTSTAMP! ioctl should be used.
\end{itemize}

This DT bindings based solution works only for platform devices.
For CTU CAN FD cores used on the PCI bus, a table vendor with vendor name and timestamping frequency will be (probably) maintained in the driver source code.

The CTU CAN FD IP core also allows to set timestamping at the start/end of frame, but there isn't an kernel interface to expose this setting to userspace.
One option is to maintain an out-of-tree driver with e.g. various module parameters, which probably wouldn't get merged into the mainline kernel.
PEAK-system do something similar with their driver\footnote{\url{https://www.peak-system.com/fileadmin/media/linux/implementation-details.html}}.
