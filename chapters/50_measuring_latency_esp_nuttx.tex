\chapter{Measuring Latency of NuttX OS Running on ESP32}
\label{chapter:nuttx}
This chapter deals with measuring latency of NuttX OS CAN driver for ESP32C3 TWAI peripheral.
My colleague Jan Charvát developed a NuttX driver for CAN controller on a ESP32-C3 board~\cite{charvat_diplomka}, and needed to verify it works correctly.
This measurement confirmed that not only it works correctly, but it is also quite fast and can handle heavy load testing.

\section{Measurement Using One CAN Bus}
During the spring semester, an opportunity arose to measure latencies on yet another CAN device.
My classmate Jan Charvát was developing a CAN driver for NuttX running on an ESP32 board.
And our supervisor came with the idea to test our projects against each other.

There is, however, one catch.
Only one CAN device is available on the ESP32 board, so the latency testing has to be slightly modified.
Figure~\ref{figure:testbed-setup-esp32} shows modified setup.
The gateway running on ESP32 retransmitts the CAN message with ID decremented by one to increase the message's priority, and ensure that the gateway will be able transmit its response, even when latester is flooding the CAN bus with its messages.
Crossbar configuration here is \ctulst!0x01000000! - tie all the CAN controllers together in one line and enable the line.

\begin{figure}
    \includegraphics[width=\linewidth]{figures/drawing_esp32.pdf}
    \caption{Modified setup with ESP32 board running NuttX as a Device Under Test (DUT). Interface can3 can be used for flooding the CAN bus with low priority traffic.}
    \label{figure:testbed-setup-esp32}
\end{figure}

% ASI NIC: jeste jeden obrazek kterej ukaze co se mysli latenci, podobnej tomu co ma byt v kapitole 2
% \begin{figure}
%     \includegraphics[width=0.8\linewidth]{figures/cmelak1.jpg}
%     \caption{Modified setup with ESP32 board running NuttX as a Device Under Test (DUT). Interface can3 can be used for flooding the CAN bus with low priority traffic.}
%     \label{figure:nuttx-latency-diagram}
% \end{figure}

\section{NuttX}

NuttX is a real time operating system with focus on being scalable (from 8-bit to 64-bit microcontrollers), compliant with standards (POSIX), highly configurable and extensible to new processor/SoC/board architectures~\cite{nuttx:doc-about}.
It has small footprint (the smallest NuttX build can fit into 32 kB of memory).
NuttX aims to be ANSI and POSIX compliant, so programming for NuttX is easier when you already know development for Linux.
Of course NuttX doesn't implement everything that Linux provides, only a small subset, to keep its small footprint.
It could be called “Linux lite”.
And it also provides some features of VxWorks (task management, watchdog timers), another real time OS developed by Wind River Systems.
The NuttX system is fully pre-emptible, supports FIFO and round-robin scheduling, sporadic events, tickless operation, and has support for priority inheritance.

NuttX manages to keep its small footprint by using a special source code tree structure.
It consists of a lot of small files, and most files contain only one function.
Those source files are then compiled into objects and saved as static libraries.
This allows the linked to include only the actually used parts from the archives into the final binary.
Also the configuration file allows you to only compile features you plan to use (similar to Linux config).
Besides that, it makes use of GNU toolchain's support for {\em weak} symbols to further help keep the footprint tiny.

From device drivers, NuttX supports character and block devices, has network drivers, USB drivers, serial, I2C, CAN bus, and much more~\cite{nuttx:doc-about}.
My colleague Jan Charvát worked on the lower-half of a CAN driver for Espressif ESP32-C3 microcontroller.

\section{Espressif ESP32-C3}
The NuttX OS runs on an Espressif ESP32-C3-Mini-1, a module based on the ESP32-C3 series SoC, running on a 32-bit single core RISC-V architecture microprocessor~\cite{espressif:esp32}.
It has WiFi and Bluetooth connectivity, accomodates 15 GPIOs (General Purpose Input/Output pins), and has built-in TWAI controller.
TWAI is practically the CAN protocol, but renamed to avoid licensing costs.
It supports only the classical CAN, not the CAN FD.

\begin{figure}
    \includegraphics[width=0.8\linewidth]{figures/esp32-c3-devkitm-1-v1-isometric.png}
    \caption{ESP32-C3 DevKit.}
    \label{figure:esp32-fotka}
\end{figure}


\section{Latester Modifications}
\label{sec:nuttx-latester-modifications}

Latester (the application used for measuring latencies) had to be modified for this new use case.
At first, only slight modification in the measurement thread in frames processing to differentiate the original frame and the responded frame with ID smaller by 1.
Then we ran the measurements, only to find out there were huge frame losses, ranging roughly from 30 \% up to 60 \% of all frames sent.
A loss means that frame was successfully sent, but latester haven't received back the same frame with modified ID.
% Then I realized logging from the measurement thread slows down the application enough to start losing frames in such a setup (in the regular setup, latester manages to process everything flawlessly, but logging from measurement thread is factually bad and is removed).

We verified that NuttX responded to all CAN frames correctly, see the note on frame loss in section~\ref{section:nuttx-results}.
So I went looking for bugs in latester and found a bug with timeouting at the end of measurement, but this didn't help with the lost frames.
We then identified that the problem is that it the system runs is not fast enough to process all the CAN frames, as there have been multiple \ctulst!rx fifo overflow! errors from the CAN controllers.
After all, the system now has to handle double the amount of interrupts from CAN frames reception, compared to the regular measurement with two CAN buses.

So acceptance filters have been used to filter frames on both reception interfaces.
Interface \ctulst!can1! is set up to ignore the modified frame ID (regular-1).
Likewise, interface \ctulst!can2! ignores the regular measurement ID.
Those filters helped to reduce processing load, because it avoids context switches between the kernel and userspace (latester no longer has to \ctulst!recvmmsg()! a CAN frame only to find out it can be ignored).

Acceptance filters can be set using SocketCAN~\cite{kernel-doc:networking-can}.
Listing~\ref{listing:socketcan-filter-example} shows an example snippet with two CAN filters.
Those filters are inverse, so only frame with ID different than the filtered passes.
However, the filter passed to \ctulst!CAN_RAW_FILTER! option are in logical \ctulst!OR!.
Which makes the two inverted filters useless.
To use the filters in logical \ctulst!AND!, a \ctulst!CAN_RAW_JOIN_FILTERS! socket option is needed.
Listing~\ref{listing:socketcan-join-filters} shows a snippet of how the join option is used.

\begin{lstlisting}[caption={Example filter setup on CAN socket, the first filter blocks messages with ID \ctulst!0xA!, the second blocks IDs \ctulst!0xB!.},label={listing:socketcan-filter-example}]
struct can_filter rfilter[2];

rfilter[0].can_id   = 0x00A | CAN_INV_FILTER;
rfilter[0].can_mask = CAN_SFF_MASK;
rfilter[1].can_id   = 0x00B | CAN_INV_FILTER;
rfilter[1].can_mask = CAN_SFF_MASK;

setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));
\end{lstlisting}

\begin{lstlisting}[caption={Using \ctulst!CAN_RAW_JOIN_FILTERS! to join the filters on socket \ctulst!s! into logical \ctulst!AND!.},label={listing:socketcan-join-filters}]
int join_filter = 1;
setsockopt(s, SOL_CAN_RAW, CAN_RAW_JOIN_FILTERS, &join_filter, sizeof(join_filter))
\end{lstlisting}

Those filters, together with making sure we are running RT patched Linux, helped to ensure that all CAN frames are and will be handled.

\section{Measurements}
We have tried to automate the measurements as much as possible, so I've developed a Python script (included in the \ctulst!can-benchmark! repository) to run parametrized tests.
The altered variables are:
\begin{itemize}
\item CAN bus speed: we used 3 different bitrates: 125k, 500k, 1M,
\item CAN frame length: 2, 4 or 8 bytes,
\item number of messages sent: 100, 1000 or 10000,
\item boolean switch indicating whether the messages are sent one by one or in “flood” mode,
\item boolean switch indicating whether NuttX had artificial load.
\end{itemize}

Artifical load on NuttX is done by running another thread which runs a busy loop and prints into the console.
This thread has lower priority than the main thread which sends CAN frames.

From the list of variables, CAN bus speed and NuttX artificial load were not automated.
So we set those things manually.
It could be done, but we didn't devote resources to it.
To automate the CAN bus speed setting, one would have to recompile the NuttX OS with new configuration, and then load the binary onto the ESP32 board.
This could be hacked together with Python's \ctulst!os! standard library.
To automate starting the busy loop on NuttX, one could use some Python library to connect NuttX's NSH shell over serial line and send a command to start the load.

Results from those automated tests were processes using another Python script.
This script gathered all awailable files with histograms, grouped them by the flood switch, bus speed, and number of messages, and generates another script (this time shell) to run \ctulst!gnuplot! commands and create .pdf files with the plots.
The generated plots are in appendix~\ref{appendix:nuttx-mereni}.
However, those measurements were done in the beginning of April, and by then we still didn't have everything figured out about both the testing and the tested parts, so those plots should be taken with a grain of salt.

We have also done a test measurement to verify NuttX scheduling and demonstrate the effect of incorrectly set process priorities.
The busy-loop process's priority has been changed to be higher than that of the CAN responder.

\section{Results}
\label{section:nuttx-results}

Table~\ref{tabulka:nuttx-oneattime} shows measured NuttX latencies when sending messages one at a time.
For bus speed 125 kb/s, the measured latency is zero, which means that NuttX managed to process the message during the time for 3 IFS (Inter Frame Space) bits.
At the 125 kb/s bitrate, one bit takes about \SI{8}{\micro\second}, so the inter frame space is \SI{24}{\micro\second}.
Therefore we can conclude that the NuttX processing latency is smaller than \SI{24}{\micro\second}, and thus is unmeasurable.
Keep in mind that “zero NuttX latency” means “zero NuttX latency when measuring on a single CAN bus”.
If we had another CAN bus available, we would measure some nonzero latency.

And judging from the results for 1 Mb/s bitrate, we can estimate the NuttX latency to be around \SI{15}{\micro\second}.
At 1 Mb/s, the inter frame space is only \SI{3}{\micro\second} (1 bit takes \SI{1}{\micro\second}).
Also the effect of frame length on processing latency can be seen here, for frame length 2 the latency is on average \SI{10}{\micro\second}, for frame length 8 it goes up to \SI{12}{\micro\second}.
For bus speed 500 kb/s, the average latencies are \SI{8}{\micro\second} (\SI{10}{\micro\second}).
As the inter frame space is \SI{6}{\micro\second}, more of the processing latency hides in the IFS time.

% Please add the following required packages to your document preamble:
% \usepackage{graphicx}
% NuttX latencies - one at a time measurement
\begin{table}[htb]
    \centering
    \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|ccl|ccl|ccl|}
    \hline
    \multicolumn{1}{|l|}{} & \multicolumn{2}{c|}{2 data bytes}                                                      & \multicolumn{2}{c|}{4 data bytes}                                                      & \multicolumn{2}{c|}{8 data bytes}                                                      \\ \hline
    bitrate                   & \multicolumn{1}{c|}{avg {[}us{]}} & \multicolumn{1}{c|}{worst {[}us{]}}  & \multicolumn{1}{c|}{avg {[}us{]}} & \multicolumn{1}{c|}{worst {[}us{]}}  & \multicolumn{1}{c|}{avg {[}us{]}} & \multicolumn{1}{c|}{worst {[}us{]}}     \\ \hline
    125 Kbps               & \multicolumn{1}{c|}{0}            & \multicolumn{1}{c|}{0}                  & \multicolumn{1}{c|}{0}            & \multicolumn{1}{c|}{0}               & \multicolumn{1}{c|}{0}            & \multicolumn{1}{c|}{0}                  \\ \hline
    500 Kbps               & \multicolumn{1}{c|}{8}            & \multicolumn{1}{c|}{10}                 & \multicolumn{1}{c|}{8}            & \multicolumn{1}{c|}{10}              & \multicolumn{1}{c|}{10}           & \multicolumn{1}{c|}{12}                 \\ \hline
    1 Mbps                 & \multicolumn{1}{c|}{10}           & \multicolumn{1}{c|}{12}                 & \multicolumn{1}{c|}{10.25}        & \multicolumn{1}{c|}{12}              & \multicolumn{1}{c|}{12}           & \multicolumn{1}{c|}{14}                 \\ \hline
    \end{tabular}%
    }
    \caption{Measured NuttX latencies, messages sent one at a time. 3200 messages were sent.}
    \label{tabulka:nuttx-oneattime}
\end{table}

Table~\ref{tabulka:nuttx-flood} shows the measured latencies when sending messages in flood mode (as fast as possible).
For bitrate 125 kb/s, the latencies are zero.
NuttX is fast enough to process the CAN message during inter frame space, and then sends back the response without problem, because the response has higher priority (ID \ctulst!0x9! vs original ID \ctulst!0xA!).
For bitrates 500 kb/s and 1 Mb/s, we can see nonzero latencies.
Those average latencies rougly correspond to the time it takes to send one CAN frame.
E.g. for bus speed 500 kb/s, one frame of length 8 is 16 bytes long (for more accurate terminology, “data length” should be used instead of “frame length”), 16 bytes is 128 bites, and 1 bite takes \SI{2}{\micro\second}.
Thus the average frame takes about \SI{256}{\micro\second} (not including bit stuffing, CRC field etc.).
Because at those bitrates NuttX won't process the message during IFS, another CAN frame is sent by the latester, and NuttX has to wait until it another CAN frame is completely send before it can send its response back.
The data back this up, as the average latency is proportionally lower for shorter frame lengths/faster bitrate.


We have also tested how NuttX handles heavy traffic load by running \ctulst!cangen!, and \ctulst!candump!-ing all CAN frames into a text file (to a ramdisk to minimize network traffic).
This log file confirmed that NuttX correctly responded to all the messages, and endured traffic of about 10k frames per second.
The command used to generate frames is \ctulst!cangen can2 -I 0xA -g 0 -i -x! (full load test ignoring "No buffers available" error).

% Please add the following required packages to your document preamble:
% \usepackage{graphicx}
% NuttX latencies - “flood” measurement
\begin{table}[htb]
    \centering
    \resizebox{\textwidth}{!}{%
    \begin{tabular}{|c|cc|cc|cc|}
    \hline
             & \multicolumn{2}{c|}{2 data bytes}                                                      & \multicolumn{2}{c|}{4 data bytes}                                                      & \multicolumn{2}{c|}{8 data bytes}                                                      \\ \hline
    freq     & \multicolumn{1}{c|}{avg {[}us{]}} & \multicolumn{1}{c|}{worst {[}us{]}} & \multicolumn{1}{c|}{avg {[}us{]}} & \multicolumn{1}{c|}{worst {[}us{]}}  & \multicolumn{1}{c|}{avg {[}us{]}} & \multicolumn{1}{c|}{worst {[}us{]}}  \\ \hline
    125 Kbps & \multicolumn{1}{c|}{0}            & \multicolumn{1}{c|}{0}              & \multicolumn{1}{c|}{0}            & \multicolumn{1}{c|}{0}               & \multicolumn{1}{c|}{0}            & \multicolumn{1}{c|}{0}               \\ \hline
    500 Kbps & \multicolumn{1}{c|}{132.5}        & \multicolumn{1}{c|}{286}            & \multicolumn{1}{c|}{170.3}        & \multicolumn{1}{c|}{178}             & \multicolumn{1}{c|}{246.2}        & \multicolumn{1}{c|}{250}             \\ \hline
    1 Mbps   & \multicolumn{1}{c|}{66}           & \multicolumn{1}{c|}{141}            & \multicolumn{1}{c|}{85.2}         & \multicolumn{1}{c|}{89}              & \multicolumn{1}{c|}{123.2}        & \multicolumn{1}{c|}{127}             \\ \hline
    \end{tabular}%
    }
    \caption{Measured NuttX latencies, messages sent a flood mode. 3200 messages were sent.}
    \label{tabulka:nuttx-flood}
\end{table}

Finally graph~\ref{figure:nuttx-compare-priorities} shows the effect of incorrectly set process priorities.
When the priorities are correct (busy loop has lower priority than the useful task), the latency is mostly zero, and only for few frames (count in lower tens) gets above zero.
When the priorities are inverted, the latency profile degraded and more than 200 messages have rather large latency - roughly around \SI{250}{\micro\second} and more.
There are also two big steps at the end (for both profiles), which means that few frames had latency in the order of milliseconds, which is quite bad.
We were not able to pinpoint why it is happening, doesn't seems like a bug in the latester (we have exact timestamps from CTU CAN FD cores, which prove that the frame appeared on bus much later).
And during regular measurement without CPU load on NuttX, such latencies were not observer.

\begin{figure}
    \includegraphics[width=0.8\linewidth]{figures/compare_priorities.pdf}
    \caption{NuttX latencies, system is under load by busy loop, showing the effect of incorrectly set priorities. Bus speed is set to 125k, frame length 2 is used, 3200 messages sent, sending messages one at a time.}
    \label{figure:nuttx-compare-priorities}
\end{figure}
